import React, {Component} from 'react';

import {Route, NavLink, Switch, Redirect} from 'react-router-dom'

import './Blog.css';
import Posts from './Posts/Posts'
//import NewPost from "./NewPost/NewPost";
import asynchComponent from "../../hoc/asynchComponent";
const AsynchNewPost = asynchComponent(() => {
    return import('./NewPost/NewPost')
})

class Blog extends Component {
    state={
        auth: true
    }

    render() {
        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink
                                activeClassName="my-active"
                                activeStyle={{
                                    color: '#fa923f',
                                    textDecoration: 'underline'
                                }}
                                to="/posts" exact>Posts</NavLink></li>
                            <li><NavLink to={{
                                pathname: '/new-post',
                                hash: '#submit',
                                search: '?quick-submit=true'
                            }}>New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>
                {/*<Route path="/" exact render={() => <h1>Home</h1>}/>
                <Route path="/new-post" exact render={() => <h1>New post</h1>}/>*/}
                <Switch>
                    {this.state.auth ? <Route path="/new-post" component={AsynchNewPost}/> : null }
                    <Route path="/posts" component={Posts}/>
                    <Route component={() => <h1>Not found</h1>}/>
                    {/*<Redirect from="/" to="/posts"/>*/}
                    {/*<Route path="/" component={Posts}/>*/}
                </Switch>
            </div>
        )
    }
}

export default Blog;